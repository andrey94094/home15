// JavaScript source code
let arr = [{ name: 'igor', age: 42 }, { name: 'lena', age: 52 }];
//---1
function arrSort(a, b) {
    if (a.age > b.age) return -1;
    if (a.age === b.age) return 0;
    if (a.age < b.age) return 1;
}
console.log(arr.sort(arrSort));
//---2
const vertyt = arr.filter(function (item) {
    return item.name === 'lena';
})
console.log(vertyt);
//---3
const ano = 'igor';
const another = arr.filter(function (item) {
    return item.name === ano;
})
console.log(another);
//---4
arr.forEach(element => console.log(element))
//---5
let arrTwo = [ ...arr ];
arrTwo.forEach(function (item) {
    if (item.age < 50) { item.age = item.age + 1 };
})
//---6
let arrThree = [5, 6, 3, 0, 'aw', 'sds', 2];
const arrFour = arrThree.map(function (item,index) {
    if (typeof item === 'number') {
        return item + index;
    }
    else { return item + item }
   return item;
});
arrFour.splice(0, 1);
console.log(arrFour);